# AfpDAQ
**This is meta repository to build AFP TDAQ and RCE code**
```sh
git clone --recurse-submodule <repo_URL> [<target_dir=afpdaq>]
source /cvmfs/atlas.cern.ch/repo/sw/tdaq/tools/cmake_tdaq/bin/cm_setup.sh [<target_TDAQ>]
cd <taget_dir>
git config core.hookspath .githooks
git checkout
cmake_config
cd $CMTCONFIG
make -j
make install -j
cd ..
```

**Testing COOL reading/writing**
```sh
source installed/setup.sh
afp_writeCool  -d 'sqlite://;schema=./mydb.db;dbname=TESTDB' -t Run3-UPD1-00-00 -a /det/afp/configs/moduleconfigs/top/run3/VLDB_2020_A__234.cfg -c /det/afp/configs/moduleconfigs/top/run3/VLDB_2020_C__234.cfg
afp_readCool -j out.json  -d 'sqlite://;schema=./mydb.db;dbname=TESTDB' -t Run3-UPD1-00-00
```
